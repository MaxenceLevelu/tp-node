const express = require('express')
const app = express()
const axios = require('axios');

app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))


app.get('/users', async function (req, res) {
    try {
        const { data } = await axios.get('https://jsonplaceholder.typicode.com/users')
        console.log(data)
        res.send(data)
    } catch (error) {
        res.send(error)
    }
})

app.get('/users/:id', async function (req, res) {
    try {
        const id = req.params.id
        const { data } = await axios.get(`https://jsonplaceholder.typicode.com/users/${id}`)
        console.log(data)
        res.send(data)
    } catch (error) {
        res.send(error)
    }
})

app.get('/users/:id/posts', async function (req, res) {
    try {
        const id = req.params.id
        const { data } = await axios.get(`https://jsonplaceholder.typicode.com/users/${id}/posts`)
        console.log(data)
        res.send(data)
    } catch (error) {
        res.send(error)
    }
})

app.get('/users/:id/albums', async function (req, res) {
    try {
        const id = req.params.id
        const user = await axios.get(`https://jsonplaceholder.typicode.com/users/${id}`)
        const albums = await axios.get(`https://jsonplaceholder.typicode.com/users/${id}/albums`)
        const photos = await axios.get(`https://jsonplaceholder.typicode.com/photos`)
        let albumPhotos = []
        for (album of albums.data) {
            albumPhotos = []
            for (photo of photos.data) {
                if (album.id == photo.albumId) {
                    albumPhotos.push(photo)
                }
            }
            album['image'] = albumPhotos
        }
        user.data['albums'] = albums.data
        console.log(user.data)
        res.send(user.data)
    } catch (error) {
        res.send(error)
    }
})

app.post('/users', async function (req, res) {
    try {
        console.log(req.body)
        const response = await axios.post('https://jsonplaceholder.typicode.com/users', {
            name: req.body.name,
            username: req.body.username,
            email: req.body.email
        })
        console.log(response)
        res.send(response.data)
    } catch (error) {
        res.send(error)
    }
})

app.put('/users/:id', async function (req, res) {
    try {
        console.log(req.body)
        const id = req.params.id
        const response = await axios.put(`https://jsonplaceholder.typicode.com/users/${id}`, {
            name: req.body.name,
            username: req.body.username,
            email: req.body.email
        })
        console.log(response)
        res.send(response.data)
    } catch (error) {
        res.send(error)
    }
})

app.delete('/users/:id', async function (req, res) {
    try {
        const id = req.params.id
        const response = await axios.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
        console.log(response.status)
        res.send(response)
    } catch (error) {
        res.send(error)
    }
})


app.listen(3000)